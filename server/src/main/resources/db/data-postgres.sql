INSERT INTO order_details(id, description, title, status, priority, email, deleted) VALUES (1, 'test', 'test title', 'SUBMITTED', 'STANDARD', 'test@test.at', false);
INSERT INTO order_details(id, description, title, status, priority, email, deleted) VALUES (2, 'test', 'test title', 'SUBMITTED', 'HIGH_PRIORITY', 'test@test.at', false);
INSERT INTO order_details(id, description, title, status, priority, email, deleted) VALUES (3, 'test', 'test title', 'SUBMITTED', 'HIGH_PRIORITY', 'flo.ces.fc@gmail.com', false);
SELECT(setval('order_details_id_seq', 3));

INSERT INTO order_form(id, order_details_id, order_submitted, deleted) VALUES (1, 1, CURRENT_TIMESTAMP, false);
INSERT INTO order_form(id, order_details_id, order_submitted, deleted) VALUES (2, 2, CURRENT_TIMESTAMP, false);
INSERT INTO order_form(id, order_details_id, order_submitted, deleted) VALUES (3, 3, CURRENT_TIMESTAMP, false);
SELECT(setval('order_form_id_seq', 3));