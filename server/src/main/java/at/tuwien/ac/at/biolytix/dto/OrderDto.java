package at.tuwien.ac.at.biolytix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class OrderDto extends BaseDto {

    public OrderDto(Integer id) {
        this.id = id;
    }

    public OrderDto() {

    }

    @JsonProperty("orderDetails")
    private OrderDetailsDto orderDetails;

    @JsonProperty("orderArrived")
    private Date orderArrived;

    @JsonProperty("orderReleased")
    private Date orderReleased;

    @JsonProperty("orderSubmitted")
    private Date orderSubmitted;

    public OrderDetailsDto getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetailsDto orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Date getOrderArrived() {
        return orderArrived;
    }

    public void setOrderArrived(Date orderArrived) {
        this.orderArrived = orderArrived;
    }

    public Date getOrderReleased() {
        return orderReleased;
    }

    public void setOrderReleased(Date orderReleased) {
        this.orderReleased = orderReleased;
    }

    public Date getOrderSubmitted() {
        return orderSubmitted;
    }

    public void setOrderSubmitted(Date orderSubmitted) {
        this.orderSubmitted = orderSubmitted;
    }
}
