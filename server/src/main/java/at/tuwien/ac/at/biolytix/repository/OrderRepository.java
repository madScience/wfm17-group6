package at.tuwien.ac.at.biolytix.repository;

import at.tuwien.ac.at.biolytix.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query("SELECT o FROM Order o WHERE o.orderArrived = null")
    List<Order> findUnarrivedOrders();

    @Query("SELECT o FROM Order o WHERE o.orderDetails.token = ?1")
    Order findOrderByOrderDetailsToken(String token);
}
