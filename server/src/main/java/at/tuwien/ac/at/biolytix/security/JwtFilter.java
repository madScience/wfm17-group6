package at.tuwien.ac.at.biolytix.security;

import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.service.impl.AccessControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.SignatureException;
import java.util.Date;

@Component
@Order()
public class JwtFilter extends GenericFilterBean {

    @Value("${application.authactive:true}")
    private boolean authactive;

    @Value("${application.authtoken:}")
    private String authtoken;

    @Autowired
    private AccessControlService accessControlService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;

        String token;

        if (authactive) {
            final String authHeader = request.getHeader("Authorization");
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                token = "";
            } else {
                token = authHeader.substring(7); // The part after "Bearer "
            }
        } else {
            token = authtoken;
        }
        if(!token.isEmpty()) {
            String username = JwtUtils.getUserNameFromToken(token);
            Date issuedAt = JwtUtils.getIssuedAtFromToken(token);
            String role = JwtUtils.getRoleFromToken(token);

            try {
                accessControlService.authenticate(token);
            } catch (ServiceException e) {
                throw new ServletException("Ungültiger Authorizationtoken.");
            }

        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
