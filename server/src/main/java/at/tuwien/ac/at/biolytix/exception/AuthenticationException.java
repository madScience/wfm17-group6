package at.tuwien.ac.at.biolytix.exception;


public class AuthenticationException extends ServiceException {

    public AuthenticationException(String message) { super(message); }
}
