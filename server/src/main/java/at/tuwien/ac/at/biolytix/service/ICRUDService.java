package at.tuwien.ac.at.biolytix.service;

import at.tuwien.ac.at.biolytix.dto.BaseDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;

import java.util.List;

public interface ICRUDService<D extends BaseDto> {

    /**
     * General method to retrieve different DTOs
     * @param toRead - containing the ID of the wanted entity
     * @return converted dto from entity
     * @throws ServiceException
     */
    D read(D toRead) throws ServiceException;

    /**
     * General method to retrieve all DTOs from DTO type D
     * @return all DTOs found for DTO type D
     * @throws ServiceException
     */
    List<D> readAll() throws ServiceException;

    /**
     * General method to create a specific DTO for type D
     * @param toCreate - DTO containing all needed information for creation
     * @return DTO of entity saved into the database
     * @throws ServiceException
     */
    D create(D toCreate) throws ServiceException;

    /**
     * General method to update a specific DTO for type D
     * @param toUpdate - DTO to contain updated information and the ID of the entity
     * @return updated DTO with new and old information
     * @throws ServiceException
     */
    D update(D toUpdate) throws ServiceException;

    /**
     * General method to delete a specific DTO for type D
     * @param toDelete - DTO to contain the ID of the entity to be deleted
     * @throws ServiceException
     */
    void delete(D toDelete) throws ServiceException;

}