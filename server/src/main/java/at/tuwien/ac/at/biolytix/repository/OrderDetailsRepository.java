package at.tuwien.ac.at.biolytix.repository;

import at.tuwien.ac.at.biolytix.model.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Integer> {
}
