package at.tuwien.ac.at.biolytix.service;

import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;

import java.util.Date;

import org.springframework.scheduling.annotation.Async;


public interface IEmailService {

    /**
     * Sends a confirmation email containing a verification token.
     * @param orderDto
     *  order containing receiver email address and token (to associate with order)
     */
    @Async
    void sendConfirmationEmail(OrderDto orderDto) throws ServiceException;

    /**
     *  Sends an arrival email containing arrival time.
     * @param orderDto
     *  order containing receiver email address
     * @param Date
     *  date containing arrival time
     */
    @Async
    void sendArrivalEmail(OrderDto orderDto, Date date) throws ServiceException;

    /**
     * Sends a release email containing a verification token.
     * @param orderDto
     *  order containing receiver email address and token (to associate with order)
     */
    @Async
    void sendReleaseEmail(OrderDto orderDto) throws ServiceException;
}
