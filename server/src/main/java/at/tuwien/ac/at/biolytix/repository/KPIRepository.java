package at.tuwien.ac.at.biolytix.repository;

import at.tuwien.ac.at.biolytix.model.KPI;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Florian
 * Date: 26.05.2017
 * Time: 13:00
 */
public interface KPIRepository extends JpaRepository<KPI, Integer> {
}
