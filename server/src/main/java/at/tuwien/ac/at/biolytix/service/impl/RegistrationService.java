package at.tuwien.ac.at.biolytix.service.impl;

import at.tuwien.ac.at.biolytix.converter.UserConverter;
import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.model.User;
import at.tuwien.ac.at.biolytix.repository.UserRepository;
import at.tuwien.ac.at.biolytix.service.IRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RegistrationService implements IRegistrationService {
    @Autowired
    private UserConverter userConverter;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional(rollbackOn = ServiceException.class)
    public UserDto createUser(UserDto toCreate) throws ServiceException {
        String password = toCreate.getPassword();

        toCreate.setPassword(passwordEncoder.encode(password));
        User toSave = userConverter.toEntity(toCreate);

        toSave = userRepository.save(toSave);

        toSave = userRepository.save(toSave);


        return userConverter.toDto(toSave);
    }
}
