package at.tuwien.ac.at.biolytix.service;

import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;

public interface IRegistrationService {

    /**
     * Creates and saves a User and sends email to confirm via token
     * @return
     *  Created and saved User
     * @throws ServiceException
     *  if validation or authorization fails
     */
    UserDto createUser(UserDto toCreate) throws ServiceException;
}
