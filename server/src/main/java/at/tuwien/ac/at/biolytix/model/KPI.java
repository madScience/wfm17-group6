package at.tuwien.ac.at.biolytix.model;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "kpi")
@Where(clause = "deleted = 'f'")
@SQLDelete(sql = "update kpi set deleted = 't' where id = ?")
public class KPI implements EntityType {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String kpi1;

    private String kpi2;

    private String kpi3;

    private boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getKpi1() {
        return kpi1;
    }

    public void setKpi1(String kpi1) {
        this.kpi1 = kpi1;
    }

    public String getKpi3() {
        return kpi3;
    }

    public void setKpi3(String kpi3) {
        this.kpi3 = kpi3;
    }

    public String getKpi2() {
        return kpi2;
    }

    public void setKpi2(String kpi2) {
        this.kpi2 = kpi2;
    }
}

