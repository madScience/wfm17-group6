package at.tuwien.ac.at.biolytix.model;


public enum OrderStatus {
    SUBMITTED("SUBMITTED"),
    APPROVED("APPROVED"),
    ANALYZED("ANALYZED"),
    FINISHED("FINISHED"),
    FAILED("FAILED");

    private final String name;

    OrderStatus(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public static OrderStatus fromName(String value) {
        for (OrderStatus s : values()) {
            if (s.toString().equalsIgnoreCase(value))
                return s;
        }
        throw new IllegalArgumentException();
    }
}
