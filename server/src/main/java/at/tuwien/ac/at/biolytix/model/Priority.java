package at.tuwien.ac.at.biolytix.model;

public enum Priority {
    STANDARD("STANDARD"),
    HIGH_PRIORITY("HIGH_PRIORITY");

    private final String name;

    Priority(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public static Priority fromName(String value) {
        for (Priority p : values()) {
            if (p.toString().equalsIgnoreCase(value))
                return p;
        }
        throw new IllegalArgumentException();
    }
}
