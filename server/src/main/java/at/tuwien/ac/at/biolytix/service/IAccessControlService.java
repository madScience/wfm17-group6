package at.tuwien.ac.at.biolytix.service;

import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.security.LoginRequest;
import at.tuwien.ac.at.biolytix.security.LoginResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;

public interface IAccessControlService extends UserDetailsService {

    /**
     * Tries to authenticate the user described in the request using the provided password.
     * @param request - containing the pw and the username
     * @param servletRequest - HttpServletRequest containing IP address of user trying to log in
     * @return
     *      A LoginResponse containing a LoginToken which can be used to authenticate the user on further requests.
     * @throws ServiceException
     *      If the authentication fails, a service exception is thrown
     */
    LoginResponse login(LoginRequest request, HttpServletRequest servletRequest) throws ServiceException, AuthenticationException;

    /**
     * Tries to re-authenticate based on an AuthenticationToken.
     * @param token
     *      A signed token containing the user's claims.
     * @throws ServiceException
     *      If the re-authentication fails
     */
    void authenticate(String token) throws ServiceException;

    /**
     * Finds the logged in user of this session.
     * @return
     *      The current logged in User.
     * @throws ServiceException
     *      If no authenticated User was found.
     */
    UserDto getCurrentUser() throws ServiceException;

    /**
     * Checks if the current User has the given role.
     * @param role
     *      The role to check against the user.
     * @return
     *      true if the user has the given role, otherwise false.
     * @throws ServiceException
     *      if no User is logged in.
     */
    boolean isAuthorized(String role) throws ServiceException;

}
