package at.tuwien.ac.at.biolytix.model;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "order_form")
@Where(clause = "deleted = 'f'")
@SQLDelete(sql = "update order set deleted = 't' where id = ?")
public class Order implements EntityType {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "order_details_id")
    private OrderDetails orderDetails;

    @Column(name = "order_submitted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderSubmitted;

    @Column(name = "order_finished")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderFinished;

    @Column(name = "order_arrived")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderArrived;

    @Column(name = "order_released")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderReleased;

    private boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Date getOrderSubmitted() {
        return orderSubmitted;
    }

    public void setOrderSubmitted(Date orderSubmitted) {
        this.orderSubmitted = orderSubmitted;
    }

    public Date getOrderFinished() {
        return orderFinished;
    }

    public void setOrderFinished(Date orderFinished) {
        this.orderFinished = orderFinished;
    }

    public Date getOrderArrived() {
        return orderArrived;
    }

    public void setOrderArrived(Date orderArrived) {
        this.orderArrived = orderArrived;
    }

    public Date getOrderReleased() {
        return orderReleased;
    }

    public void setOrderReleased(Date orderReleased) {
        this.orderReleased = orderReleased;
    }
}
