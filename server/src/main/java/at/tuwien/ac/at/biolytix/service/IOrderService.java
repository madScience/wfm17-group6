package at.tuwien.ac.at.biolytix.service;

import at.tuwien.ac.at.biolytix.dto.KPIDto;
import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;

import java.util.List;

public interface IOrderService extends ICRUDService<OrderDto> {

    OrderDto checkForUnarrivedOrder() throws ServiceException;

    Boolean updateArrivedOrder(int id) throws ServiceException;

    Boolean updateAnalysisSuccessfull(int id) throws ServiceException;

    KPIDto generateKpis() throws ServiceException;

    Boolean updateReviewedAndRelease(int id) throws ServiceException;

    OrderDto getByToken(String token) throws ServiceException;

    void normalizeKpis();
}
