package at.tuwien.ac.at.biolytix.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtils {

    @Value("${application.secretKey:}")
    public String _secretKey;
    private static String secretKey;

    @PostConstruct
    public void init() {
        secretKey = _secretKey;
    }

    public static String createToken(org.springframework.security.core.userdetails.UserDetails userDetails) {
        String username = userDetails.getUsername();

        Map<String, Object> claims = new HashMap<>();
        claims.put("role", userDetails.getAuthorities().iterator().next().getAuthority());
        claims.put("username", username);

        return Jwts.builder()
                .setSubject(username)
                .setClaims(claims)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, secretKey).compact();
    }

    public static String getUserNameFromToken(String token) {
        return getClaims(token).get("username", String.class);
    }

    public static Date getIssuedAtFromToken(String token){
        return getClaims(token).get("iat",Date.class);
    }

    public static String getRoleFromToken(String token) { return getClaims(token).get("role", String.class); }

    public static UserDetails getUserdetailsFromToken(String token){
        return new UserDetails(JwtUtils.getUserNameFromToken(token), JwtUtils.getRoleFromToken(token));
    }

    private static Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(secretKey)
                .parseClaimsJws(token).getBody();
    }
}
