package at.tuwien.ac.at.biolytix.converter;

import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.model.User;
import at.tuwien.ac.at.biolytix.model.UserRole;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends Converter<UserDto, User> {

    private PasswordEncoder passwordEncoder;

    public UserConverter (PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected User newEntity() {
        return new User();
    }

    @Override
    protected UserDto newDto() {
        return new UserDto();
    }

    @Override
    public UserDto toDto(User entity) {
        UserDto dto = super.toDto(entity);
        dto.setRole(entity.getUserRole().toString());
        dto.setPassword(null);
        return dto;
    }

    @Override
    public User toEntity(UserDto dto) {
        User entity = super.toEntity(dto);
        entity.setUserRole(UserRole.fromName(dto.getRole()));
        return entity;
    }

    @Override
    public User toEntity(UserDto dto, User oldUser) {
        User entity = toEntity(dto);

        if (dto.getPassword() != null) {
            entity.setPassword(passwordEncoder.encode(dto.getPassword()));
        } else {
            entity.setPassword(oldUser.getPassword());
        }

        if (dto.getFirstName() != null)
            entity.setFirstName(dto.getFirstName());
        else
            entity.setFirstName(oldUser.getFirstName());

        if (dto.getLastName() != null)
            entity.setLastName(dto.getLastName());
        else
            entity.setLastName(oldUser.getLastName());

        return entity;
    }
}
