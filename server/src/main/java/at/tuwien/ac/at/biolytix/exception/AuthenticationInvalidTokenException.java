package at.tuwien.ac.at.biolytix.exception;

public class AuthenticationInvalidTokenException extends AuthenticationException {
    public AuthenticationInvalidTokenException(String message) {
        super(message);
    }
}
