package at.tuwien.ac.at.biolytix.controller;


import at.tuwien.ac.at.biolytix.dto.KPIDto;
import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.service.impl.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "order", description = "REST API Endpoint for Orders")
@RestController
@RequestMapping(value = "order")
public class OrderController extends GenericCRUDController<OrderDto> {

    private final OrderService orderService;

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);


    @Autowired
    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }

    @Override
    protected OrderDto createDtoFromId(Integer id) {
        logger.info("Creating new order");
        return new OrderDto(id);
    }

    @RequestMapping(value = "/getUnarrivedOrder", method = RequestMethod.GET)
    public OrderDto GetUnarrivedOrder() {
        logger.info("Retrieving unarrived orders");
        OrderDto order = new OrderDto();
        try {
            order = orderService.checkForUnarrivedOrder();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return order;
    }

    @RequestMapping(value = "/{id}/updateArrivedOrder", method = RequestMethod.PUT)
    public boolean UpdateArrivedOrder(int id) {

        try {
            return orderService.updateArrivedOrder(id);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @RequestMapping(value = "/{id}/updateAnalysisSuccess", method = RequestMethod.PUT)
    public boolean updateAnalysisSuccessfull(int id) {
        logger.info("ANALYSIS "+ id +" SUCCESSFULL");

        try {
            return orderService.updateAnalysisSuccessfull(id);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    @ApiOperation(value = "Get order with current status by token")
    @RequestMapping(value = "/token/{token}", method = RequestMethod.GET)
    public OrderDto getOrderByToken(@ApiParam(value = "Token") @PathVariable String token) throws ServiceException {
        return orderService.getByToken(token);
    }

    @RequestMapping(value ="/{id}/reviewedAndRelease", method = RequestMethod.PUT)
    public Boolean updateReviewAndReleaseStatus( int id) {
        logger.info("Updating the review and release status of the order with the ID: " + id);
        try {
            System.out.println("Received review and release for ID: " + id);
            return orderService.updateReviewedAndRelease(id);
        } catch (Exception ex) {
            return false;
        }
    }

    @RequestMapping(value = "/generateKPIs", method = RequestMethod.PUT)
    public KPIDto generateKPIs() {
        try {
            return orderService.generateKpis();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/normalizeKPIs", method = RequestMethod.PUT)
    public void normalizeKPIs() {
        orderService.normalizeKpis();
    }

}