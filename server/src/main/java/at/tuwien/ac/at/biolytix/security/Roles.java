package at.tuwien.ac.at.biolytix.security;

import java.util.ArrayList;
import java.util.List;

public class Roles {
    public static final String USER = "ROLE_USER";
    public static final String ADMIN = "ROLE_ADMIN";

    public static List<String> list() {
        List<String> roles = new ArrayList<>();
        roles.add(USER);
        roles.add(ADMIN);
        return roles;
    }
}
