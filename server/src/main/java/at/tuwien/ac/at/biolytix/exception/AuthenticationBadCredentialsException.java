package at.tuwien.ac.at.biolytix.exception;

public class AuthenticationBadCredentialsException extends AuthenticationException {

    public AuthenticationBadCredentialsException(String message) { super(message); }

}

