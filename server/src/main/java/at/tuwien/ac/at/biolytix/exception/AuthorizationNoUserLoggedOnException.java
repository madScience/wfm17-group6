package at.tuwien.ac.at.biolytix.exception;

public class AuthorizationNoUserLoggedOnException extends AuthenticationException {

    public AuthorizationNoUserLoggedOnException(String message) {
        super(message);
    }
}
