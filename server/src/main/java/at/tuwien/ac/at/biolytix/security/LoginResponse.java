package at.tuwien.ac.at.biolytix.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse {

    @JsonProperty(value = "token")
    private String token;

    public LoginResponse(String token) {
        this.token = token;
    }

    public LoginResponse() {}

    public String getToken() { return this.token; }
}
