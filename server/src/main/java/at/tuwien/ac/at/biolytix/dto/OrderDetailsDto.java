package at.tuwien.ac.at.biolytix.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderDetailsDto extends BaseDto {
    private String description;

    private String title;

    private String status;

    @JsonProperty(required = true)
    private String priority;

    @JsonProperty(required = true)
    private String email;

    @JsonProperty()
    private String token;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
