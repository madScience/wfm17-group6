package at.tuwien.ac.at.biolytix.converter;

import at.tuwien.ac.at.biolytix.dto.BaseDto;
import at.tuwien.ac.at.biolytix.model.EntityType;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public abstract class Converter<D extends BaseDto, E extends EntityType> {

    protected abstract E newEntity();
    protected abstract D newDto();

    public E toEntity(D dto){
        E entity = newEntity();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }

    /**
     * Converts a dto to an entity and copies references from an oldEntity
     * @param dto dto to convert
     * @param oldEntity entity references will be taken from
     * @return new entitry based on dto and oldEntity
     */
    public E toEntity(D dto, E oldEntity) {
        return toEntity(dto);
    }

    public D toDto(E entity){
        D dto = newDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    public List<E> toEntityList(List<D> dtoList){
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<D> toDtoList(List<E> entityList){
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}
