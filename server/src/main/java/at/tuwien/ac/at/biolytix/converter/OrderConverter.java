package at.tuwien.ac.at.biolytix.converter;


import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderConverter extends Converter<OrderDto, Order> {

    private OrderDetailsConverter orderDetailsConverter;

    @Autowired
    public OrderConverter(OrderDetailsConverter orderDetailsConverter) {
        this.orderDetailsConverter = orderDetailsConverter;
    }

    @Override
    public OrderDto toDto(Order entity) {
        OrderDto dto = newDto();
        dto.setOrderDetails(orderDetailsConverter.toDto(entity.getOrderDetails()));
        dto.setId(entity.getId());
        dto.setOrderArrived(entity.getOrderArrived());
        dto.setOrderReleased(entity.getOrderReleased());
        dto.setOrderSubmitted(entity.getOrderSubmitted());
        return dto;
    }

    @Override
    public Order toEntity(OrderDto dto) {
        Order entity = newEntity();
        entity.setDeleted(false);
        entity.setOrderDetails(orderDetailsConverter.toEntity(dto.getOrderDetails()));
        entity.setId(dto.getId());
        entity.setOrderArrived(dto.getOrderArrived());
        entity.setOrderReleased(dto.getOrderReleased());
        entity.setOrderSubmitted(dto.getOrderSubmitted());
        return entity;
    }

    @Override
    protected Order newEntity() {
        return new Order();
    }

    @Override
    protected OrderDto newDto() {
        return new OrderDto();
    }
}
