package at.tuwien.ac.at.biolytix.service.impl;

import at.tuwien.ac.at.biolytix.converter.UserConverter;
import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.*;
import at.tuwien.ac.at.biolytix.model.User;
import at.tuwien.ac.at.biolytix.repository.UserRepository;
import at.tuwien.ac.at.biolytix.security.JwtUtils;
import at.tuwien.ac.at.biolytix.security.LoginRequest;
import at.tuwien.ac.at.biolytix.security.LoginResponse;
import at.tuwien.ac.at.biolytix.security.UserDetails;
import at.tuwien.ac.at.biolytix.service.IAccessControlService;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Service
public class AccessControlService implements IAccessControlService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserConverter userConverter;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = loadByUsername(s);
        return new UserDetails(user);
    }

    private User loadByUsername(String authentication) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(authentication);
        if (user == null) throw new UsernameNotFoundException("Username not found!");

        return user;
    }

    @Override
    public LoginResponse login(LoginRequest request, HttpServletRequest servletRequest) throws ServiceException {
        UsernamePasswordAuthenticationToken authenticationToken;

        authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());

        Authentication authentication;

        try {
            authentication = this.authenticationManager.authenticate(authenticationToken);
        } catch (LockedException | DisabledException e) {
            throw new AuthenticationUserLockedException();
        } catch (AuthenticationException e) {
            throw new AuthenticationBadCredentialsException(e.getMessage());
        }

        User u = loadByUsername(request.getUsername());

        if (u == null) {
            throw new AuthenticationBadCredentialsException("Authentication failed!");
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails userDetails = loadUserByUsername(request.getUsername());
        return new LoginResponse(JwtUtils.createToken(userDetails));
    }

    @Override
    public void authenticate(String token) throws ServiceException {
        try {
            Date issuedAt = JwtUtils.getIssuedAtFromToken(token);
            LocalDate createdOn = issuedAt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            if(createdOn.plusYears(1).compareTo(LocalDate.now()) < 0)
                throw new AuthenticationInvalidTokenException("Invalid token!");

            UserDetails userDetails = JwtUtils.getUserdetailsFromToken(token);
            UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (JwtException e) {
            throw new AuthenticationInvalidTokenException("Invalid token!");
        }
    }

    @Override
    public UserDto getCurrentUser() throws ServiceException {
        if (!isAuthenticated()) throw new AuthorizationNoUserLoggedOnException("User is not authenticated!");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user  = (UserDetails)authentication.getPrincipal();
        User u = loadByUsername(user.getUsername());

        return userConverter.toDto(u);
    }

    @Override
    public boolean isAuthorized(String role) throws ServiceException {
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null){
            throw new AuthorizationNoUserLoggedOnException("User not logged in!");
        }
        for(GrantedAuthority g : authentication.getAuthorities()){
            if(g.getAuthority().equals(role))
                return true;
        }
        return false;
    }

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        return !(principal instanceof String && principal.equals("anonymousUser"));
    }
}
