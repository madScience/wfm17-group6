package at.tuwien.ac.at.biolytix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by IntelliJ IDEA.
 * User: Florian
 * Date: 26.05.2017
 * Time: 12:54
 */
public class KPIDto extends BaseDto {

    @JsonProperty("kpi1")
    private String kpi1;

    @JsonProperty("kpi2")
    private String kpi2;

    @JsonProperty("kpi3")
    private String kpi3;


    public String getKpi1() {
        return kpi1;
    }

    public void setKpi1(String kpi1) {
        this.kpi1 = kpi1;
    }

    public String getKpi2() {
        return kpi2;
    }

    public void setKpi2(String kpi2) {
        this.kpi2 = kpi2;
    }

    public String getKpi3() {
        return kpi3;
    }

    public void setKpi3(String kpi3) {
        this.kpi3 = kpi3;
    }
}
