package at.tuwien.ac.at.biolytix.controller;

import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.security.LoginRequest;
import at.tuwien.ac.at.biolytix.security.LoginResponse;
import at.tuwien.ac.at.biolytix.service.impl.AccessControlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(value="/account", description = "Main Controller for logging in")
@RestController
@RequestMapping(value = "/account")
public class AccountController {

    private AccessControlService accessControlService;

    public AccountController(AccessControlService accessControlService) {
        this.accessControlService = accessControlService;
    }

    @ApiOperation(value = "Tries to log in user with given credentials")
    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseEntity<LoginResponse> login(@RequestBody final LoginRequest login, HttpServletRequest request) throws ServiceException {
        LoginResponse response = accessControlService.login(login, request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieve the currently authenticated user")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UserDto> get() throws AuthorizationServiceException, ServiceException {
        UserDto user = accessControlService.getCurrentUser();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
