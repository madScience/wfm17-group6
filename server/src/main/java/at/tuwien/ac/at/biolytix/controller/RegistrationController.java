package at.tuwien.ac.at.biolytix.controller;

import at.tuwien.ac.at.biolytix.dto.UserDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.service.impl.RegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "registration", description = "Register a User")
@RestController
@RequestMapping(value = "registration")
public class RegistrationController {


    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @ApiOperation(value = "Create a new user")
    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto toCreate) throws ServiceException {
        UserDto toReturn = registrationService.createUser(toCreate);
        return new ResponseEntity<>(toReturn, HttpStatus.OK);
    }
}
