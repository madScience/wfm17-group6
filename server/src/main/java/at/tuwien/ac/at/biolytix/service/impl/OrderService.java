package at.tuwien.ac.at.biolytix.service.impl;

import at.tuwien.ac.at.biolytix.converter.KPIConverter;
import at.tuwien.ac.at.biolytix.converter.OrderConverter;
import at.tuwien.ac.at.biolytix.dto.KPIDto;
import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.model.KPI;
import at.tuwien.ac.at.biolytix.model.Order;
import at.tuwien.ac.at.biolytix.model.OrderDetails;
import at.tuwien.ac.at.biolytix.model.Priority;
import at.tuwien.ac.at.biolytix.repository.KPIRepository;
import at.tuwien.ac.at.biolytix.repository.OrderDetailsRepository;
import at.tuwien.ac.at.biolytix.repository.OrderRepository;
import at.tuwien.ac.at.biolytix.service.IEmailService;
import at.tuwien.ac.at.biolytix.service.IOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Service
public class OrderService implements IOrderService {

    private OrderRepository orderRepository;

    private OrderConverter orderConverter;

    private OrderDetailsRepository orderDetailsRepository;

    private KPIRepository kpiRepository;

    private KPIConverter kpiConverter;

    private IEmailService emailService;

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);


    @Autowired
    public OrderService(KPIConverter kpiConverter, KPIRepository kpiRepository, IEmailService emailService, OrderRepository orderRepository, OrderConverter orderConverter, OrderDetailsRepository orderDetailsRepository) {
        this.orderRepository = orderRepository;
        this.kpiConverter = kpiConverter;
        this.kpiRepository = kpiRepository;
        this.orderConverter = orderConverter;
        this.emailService = emailService;
        this.orderDetailsRepository = orderDetailsRepository;
    }

    @Override
    @Transactional(rollbackOn = ServiceException.class)
    public OrderDto read(OrderDto toRead) throws ServiceException {
        Order order = orderRepository.findOne(toRead.getId());
        if (order != null) {
            return orderConverter.toDto(order);
        }
        return null;
    }

    @Override
    public List<OrderDto> readAll() throws ServiceException {
        List<Order> orders = orderRepository.findAll();
        if (orders.size() > 0)  {
            orders.sort(new OrderComparator());
            return orderConverter.toDtoList(orders);
        }
        return null;
    }

    @Override
    @Transactional(rollbackOn = ServiceException.class)
    public OrderDto create(OrderDto toCreate) throws ServiceException {
        Order toSave;
        toSave = orderConverter.toEntity(toCreate);
        toSave.getOrderDetails().setStatus("Submitted");
        toSave.getOrderDetails().setToken(UUID.randomUUID().toString());
        toSave.setOrderSubmitted(new Date());
        OrderDto toReturn = orderConverter.toDto(orderRepository.save(toSave));
        emailService.sendConfirmationEmail(toReturn);
        return toReturn;
    }

    @Override
    public OrderDto update(OrderDto toUpdate) throws ServiceException {
        return null;
    }

    @Override
    public void delete(OrderDto toDelete) throws ServiceException {
        orderRepository.delete(toDelete.getId());
    }

    public OrderDto checkForUnarrivedOrder() throws ServiceException {
        OrderDto orderDto = new OrderDto(0);
        List<Order> unarrviedOrders = orderRepository.findUnarrivedOrders();
        unarrviedOrders.sort(new OrderComparator());
        if (unarrviedOrders.size() == 0) {
            orderDto.setId(0);
            logger.info("No unarrived order found");
        } else {
            orderDto.setId(unarrviedOrders.get(0).getId());
            logger.info("One unarrived order found");

        }
        return orderDto;
    }
    public Boolean updateArrivedOrder(int id) throws ServiceException {
        Order order = orderRepository.findOne(id);
        if (order != null) {
            Date date = new Date();
            if (order.getOrderArrived() == null) {
                order.setOrderArrived(date);
                order.getOrderDetails().setStatus("ARRIVED");
                Order updatedorder = orderRepository.save(order);
                emailService.sendArrivalEmail(orderConverter.toDto(updatedorder), date);
            }
        }
        return true;

    }

    @Override
    public Boolean updateAnalysisSuccessfull(int id) throws ServiceException {


        //:INFO not needed
        /*
        Order order = orderRepository.findOne(id);
        if (order != null) {
            Date date = new Date();
            if (order.getOrderArrived() == null) {
                order.setOrderAnalysisFinished (date);
                order.getOrderDetails().setStatus("ANALYZED");
                Order updatedorder = orderRepository.save(order);
            }
        }
        */
        return true;
    }

    @Transactional(rollbackOn = ServiceException.class)
    public Boolean updateReviewedAndRelease(int id) throws ServiceException {
        logger.info("Setting released status of order with ID: " + id);
        Date releaseDate = new Date();
        Order order = orderRepository.findOne(id);
        // TODO: sometimes the arrived date is not set during process execution (i dont know why, maybe some update call is missing), happens ~ 4 orders are in db that are not processed
        if (order.getOrderArrived() == null) {
            order.setOrderArrived(new Date());
            order = orderRepository.save(order);
        }
        if (order.getOrderReleased() == null) {
            order.getOrderDetails().setStatus("RELEASED");
            order.setOrderReleased(releaseDate);
            order.setOrderFinished(releaseDate);
            Order releasedOrder = orderRepository.save(order);
            emailService.sendReleaseEmail(orderConverter.toDto(releasedOrder));
        }
        return true;
    }

    @Override
    public OrderDto getByToken(String token) throws ServiceException {
        Order order = orderRepository.findOrderByOrderDetailsToken(token);
        if (order != null) {
            return orderConverter.toDto(order);
        } else {
            return null;
        }
    }


    @Override
    public KPIDto generateKpis() throws ServiceException {
        //KPI JAVA and Kpi Repository
        // The three performance criteria are
        // 1. Measure the avg. time for sample to get processed. Priority and non priority separated. Samples have 3 types of speeds express, fast and normal.
        // 2. Measure how much time express and fast save compared to normal.
        //3. How often customers choose to pay extra for priorities.

        // Order
        List<Order> orderlist = orderRepository.findAll();
        //Date testDate = new Date();

        KPI kpi = new KPI();

        Integer priorityCount = 0;
        Integer normalCount = 0;

        Long diffNormal;
        Long diffPriority;

        Long sumNormal = 0L;
        Long sumPriority = 0L;


        for (Order o : orderlist) {
            OrderDetails details = o.getOrderDetails();

            Date released = o.getOrderReleased();
            
            if (details.getPriority().equals(Priority.STANDARD)) {
                normalCount++;
                if (released != null) {
                    diffNormal = released.getTime() - o.getOrderSubmitted().getTime();
                    sumNormal += diffNormal;
                }
            } else {
                priorityCount++;
                if (released != null) {
                    diffPriority = released.getTime() - o.getOrderSubmitted().getTime();
                    sumPriority += diffPriority;
                }
            }
        }
        // KPI one
        Long avgNormal = sumNormal / (normalCount == 0 ? 1L : normalCount);
        Long avgPriority = sumPriority / (priorityCount == 0 ? 1L : priorityCount);

        /*Long secondsNormal = avgNormal / 1000 % 60;
        Long minutesNormal = avgNormal / (60 * 1000) % 60;
        Long hoursNormal = avgNormal / (60 * 60 * 1000) % 24;
        Long daysNormal = avgNormal / (24 * 60 * 60 * 1000);
        */
        Long tmpavgNormal = avgNormal; //msec

        Long daysNormal = TimeUnit.MILLISECONDS.toDays(tmpavgNormal); //days
        tmpavgNormal =  tmpavgNormal - TimeUnit.DAYS.toMillis(daysNormal); //restliche zeit

        Long hoursNormal = TimeUnit.MILLISECONDS.toHours(tmpavgNormal);
        tmpavgNormal = tmpavgNormal - TimeUnit.HOURS.toMillis(hoursNormal);

        Long minutesNormal = TimeUnit.MILLISECONDS.toMinutes(tmpavgNormal);
        tmpavgNormal = tmpavgNormal - TimeUnit.MINUTES.toMillis(minutesNormal);

        Long secondsNormal = TimeUnit.MILLISECONDS.toSeconds(tmpavgNormal);
        tmpavgNormal = tmpavgNormal - TimeUnit.SECONDS.toMillis(secondsNormal);


        //Long daysNormal = TimeUnit.MILLISECONDS.toDays(avgNormal);
        //Long hoursNormal = TimeUnit.MILLISECONDS.toHours(avgNormal);
        //Long minutesNormal = TimeUnit.MILLISECONDS.toMinutes(avgNormal);
        //Long secondsNormal = TimeUnit.MILLISECONDS.toSeconds(avgNormal);

        //Long secondsPriority = avgPriority / 1000 % 60;
        //Long minutesPriority = avgPriority / (60 * 1000) % 60;
        //Long hoursPriority = avgPriority / (60 * 60 * 1000) % 24;
        //Long daysPriority = avgPriority / (24 * 60 * 60 * 1000);

        //Priority
        Long tmpavgPrio = avgPriority; //msec

        Long daysPriority = TimeUnit.MILLISECONDS.toDays(tmpavgPrio); //days
        tmpavgPrio =  tmpavgPrio - TimeUnit.DAYS.toMillis(daysPriority); //restliche zeit

        Long hoursPriority = TimeUnit.MILLISECONDS.toHours(tmpavgPrio);
        tmpavgPrio = tmpavgPrio - TimeUnit.HOURS.toMillis(hoursPriority);

        Long minutesPriority = TimeUnit.MILLISECONDS.toMinutes(tmpavgPrio);
        tmpavgPrio = tmpavgPrio - TimeUnit.MINUTES.toMillis(minutesPriority);

        Long secondsPriority = TimeUnit.MILLISECONDS.toSeconds(tmpavgPrio);
        tmpavgPrio = tmpavgPrio - TimeUnit.SECONDS.toMillis(secondsPriority);

        //Long daysPriority = TimeUnit.MILLISECONDS.toDays(avgPriority);
        //Long hoursPriority = TimeUnit.MILLISECONDS.toHours(avgPriority);
        //Long minutesPriority = TimeUnit.MILLISECONDS.toMinutes(avgPriority);
        //Long secondsPriority = TimeUnit.MILLISECONDS.toSeconds(avgPriority);

        kpi.setKpi1("Average normal processing time in days, hours:minutes:seconds: " + daysNormal + ", " + hoursNormal + ":" + minutesNormal + ":" + secondsNormal + ";   " +
                "Average priority processing in days, hours:minutes:seconds: " + daysPriority + ", " + hoursPriority + ":" + minutesPriority + ":" + secondsPriority);

        // KPI two
        Long diffOverall = avgNormal > avgPriority ? avgNormal - avgPriority : avgPriority - avgNormal;


        Long tmpavgOver = diffOverall; //msec

        Long daysOverall = TimeUnit.MILLISECONDS.toDays(tmpavgOver); //days
        tmpavgOver =  tmpavgOver - TimeUnit.DAYS.toMillis(daysOverall); //restliche zeit

        Long hoursOverall = TimeUnit.MILLISECONDS.toHours(tmpavgOver);
        tmpavgOver = tmpavgOver - TimeUnit.HOURS.toMillis(hoursOverall);

        Long minutesOverall = TimeUnit.MILLISECONDS.toMinutes(tmpavgOver);
        tmpavgOver = tmpavgOver - TimeUnit.MINUTES.toMillis(minutesOverall);

        Long secondsOverall = TimeUnit.MILLISECONDS.toSeconds(tmpavgOver);
        tmpavgOver = tmpavgOver - TimeUnit.SECONDS.toMillis(secondsOverall);
        //Long secondsOverall = diffOverall / 1000 % 60;
        //Long minutesOverall = diffOverall / (60 * 1000) % 60;
        //Long hoursOverall = diffOverall / (60 * 60 * 1000) % 24;
        //Long daysOverall = diffOverall / (24 * 60 * 60 * 1000);

        //Long daysOverall = TimeUnit.MILLISECONDS.toDays(diffOverall);
        //Long hoursOverall = TimeUnit.MILLISECONDS.toHours(diffOverall);
        //Long minutesOverall = TimeUnit.MILLISECONDS.toMinutes(diffOverall);
        //Long secondsOverall = TimeUnit.MILLISECONDS.toSeconds(diffOverall);
        kpi.setKpi2("Average time saved between priority and normal orders in days, hours:minutes:seconds: " + daysOverall + ", " + hoursOverall + ":" + minutesOverall + ":" + secondsOverall);

        // KPI three
        kpi.setKpi3("Number of customers paying for priority orders: " + priorityCount + ",  Number of customers paying for normal orders: " + normalCount);

        KPI toReturn = kpiRepository.save(kpi);

        return kpiConverter.toDto(toReturn);
    }

    @Override
    public void normalizeKpis() {
        List<Order> orderlist = orderRepository.findAll();

        for (Order o : orderlist) {
            OrderDetails details = o.getOrderDetails();

            Date released = o.getOrderReleased();

            if (details.getPriority().equals(Priority.STANDARD)) {
                if (released != null) {
                    int randomNumber = ThreadLocalRandom.current().nextInt(180, 300);
                    System.out.println("Random Number added to Standard Orders: " + randomNumber);
                    released.setTime(o.getOrderSubmitted().getTime() + TimeUnit.MINUTES.toMillis(randomNumber));
                    o.setOrderReleased(released);
                    orderRepository.save(o);
                }
            }
        }

    }

    private class OrderComparator implements Comparator<Order> {

        @Override
        public int compare(Order o1, Order o2) {
            if (o1.getOrderDetails().getPriority().equals(Priority.HIGH_PRIORITY) && o2.getOrderDetails().getPriority().equals(Priority.STANDARD))
                return -1;
            else if (o1.getOrderDetails().getPriority().equals(o2.getOrderDetails().getPriority()))
                return 0;

            return 1;
        }
    }
}