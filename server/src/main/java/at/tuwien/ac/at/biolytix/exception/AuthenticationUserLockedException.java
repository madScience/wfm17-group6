package at.tuwien.ac.at.biolytix.exception;


public class AuthenticationUserLockedException extends AuthenticationException {

    public AuthenticationUserLockedException() {
        super("Dieser Benutzer ist gesperrt.");
    }

}
