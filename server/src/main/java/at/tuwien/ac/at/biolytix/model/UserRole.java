package at.tuwien.ac.at.biolytix.model;

public enum UserRole {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public static UserRole fromName(String value) {
        for (UserRole r : values()) {
            if (r.toString().equalsIgnoreCase(value))
                return r;
        }
        throw new IllegalArgumentException();
    }
}
