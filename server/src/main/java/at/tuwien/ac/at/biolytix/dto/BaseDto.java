package at.tuwien.ac.at.biolytix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class BaseDto {

    @JsonProperty("id")
    @ApiModelProperty(required = true)
    protected Integer id;

    public void setId(Integer id){
        this.id = id;
    }

    public Integer getId(){
        return id;
    }
}
