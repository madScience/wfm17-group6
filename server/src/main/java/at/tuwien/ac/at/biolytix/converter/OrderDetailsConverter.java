package at.tuwien.ac.at.biolytix.converter;

import at.tuwien.ac.at.biolytix.dto.OrderDetailsDto;
import at.tuwien.ac.at.biolytix.model.OrderDetails;
import at.tuwien.ac.at.biolytix.model.Priority;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailsConverter extends Converter<OrderDetailsDto, OrderDetails> {

    @Override
    public OrderDetailsDto toDto(OrderDetails entity) {
        OrderDetailsDto dto = newDto();
        dto.setDescription(entity.getDescription());
        dto.setTitle(entity.getTitle());
        dto.setStatus(entity.getStatus());
        dto.setId(entity.getId());
        dto.setPriority(entity.getPriority().toString());
        dto.setEmail(entity.getEmail());
        dto.setToken(entity.getToken());
        return dto;
    }

    @Override
    public OrderDetails toEntity(OrderDetailsDto dto) {
        OrderDetails entity = newEntity();
        entity.setDeleted(false);
        entity.setDescription(dto.getDescription());
        entity.setStatus(dto.getStatus());
        entity.setTitle(dto.getTitle());
        entity.setId(dto.getId());
        entity.setPriority(Priority.fromName(dto.getPriority()));
        entity.setEmail(dto.getEmail());
        entity.setToken(dto.getToken());
        return entity;
    }

    @Override
    protected OrderDetails newEntity() {
        return new OrderDetails();
    }

    @Override
    protected OrderDetailsDto newDto() {
        return new OrderDetailsDto();
    }
}
