package at.tuwien.ac.at.biolytix.converter;

import at.tuwien.ac.at.biolytix.dto.KPIDto;
import at.tuwien.ac.at.biolytix.model.KPI;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: Florian
 * Date: 26.05.2017
 * Time: 12:55
 */
@Component
public class KPIConverter extends Converter<KPIDto, KPI> {


    @Override
    protected KPI newEntity() {
        return new KPI();
    }

    @Override
    protected KPIDto newDto() {
        return new KPIDto();
    }
}
