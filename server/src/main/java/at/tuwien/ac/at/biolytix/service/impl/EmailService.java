package at.tuwien.ac.at.biolytix.service.impl;

import at.tuwien.ac.at.biolytix.dto.OrderDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.service.IEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Locale;

@Service
public class EmailService implements IEmailService {

    private JavaMailSender mailSender;

    private SpringTemplateEngine templateEngine;

    @Autowired
    public EmailService(JavaMailSender javaMailSender, SpringTemplateEngine springTemplateEngine) {
        this.mailSender = javaMailSender;
        this.templateEngine = springTemplateEngine;
    }

    @Value("${mail.from}")
    private String from;

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Override
    @Transactional(rollbackOn = ServiceException.class)
    public void sendConfirmationEmail(OrderDto orderDto) throws ServiceException {

        Locale locale = LocaleContextHolder.getLocale();

        final Context ctx = new Context(locale);

        ctx.setVariable("orderDate", new Date());

        MimeMessage mimeMessage;

        try {

            mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setSubject("Order Confirmation - Biolytix");
            message.setFrom(from);
            message.setTo(orderDto.getOrderDetails().getEmail());


            String orderToken = orderDto.getOrderDetails().getToken();

            ctx.setVariable("orderToken", orderToken);

            final String htmlContent = templateEngine.process("token-mail", ctx);
            message.setText(htmlContent, true);


            this.mailSender.send(mimeMessage);
        } catch (MessagingException ex) {
            logger.error(ex.getMessage());
        }

    }
        @Override
        @Transactional(rollbackOn = ServiceException.class)

        public void sendArrivalEmail(OrderDto orderDto, Date date) throws ServiceException {

        Locale locale = LocaleContextHolder.getLocale();

        final Context ctx = new Context(locale);

        ctx.setVariable("arrivalDate", date);

        MimeMessage mimeMessage;

        try {

            mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");

            message.setSubject("Order Arrival - Biolytix");
            message.setFrom(from);
            message.setTo(orderDto.getOrderDetails().getEmail());

            String orderToken = orderDto.getOrderDetails().getToken();

            ctx.setVariable("orderToken", orderToken);

            final String htmlContent = templateEngine.process("arrival-mail", ctx);

            message.setText(htmlContent, true);

            this.mailSender.send(mimeMessage);
        } catch (MessagingException ex) {
            logger.error(ex.getMessage());
        }
    }
    @Override
    @Transactional(rollbackOn = ServiceException.class)
    public void sendReleaseEmail(OrderDto orderDto) throws ServiceException {

        Locale locale = LocaleContextHolder.getLocale();

        final Context ctx = new Context(locale);
        ctx.setVariable("releaseDate", orderDto.getOrderReleased());

        MimeMessage mimeMessage;


        try {

            mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            message.setSubject("Order Release - Biolytix");
            message.setFrom(from);
            message.setTo(orderDto.getOrderDetails().getEmail());

            String orderToken = orderDto.getOrderDetails().getToken();

            ctx.setVariable("orderToken", orderToken);

            final String htmlContent = templateEngine.process("release-mail", ctx);

            message.setText(htmlContent, true);

            this.mailSender.send(mimeMessage);
        } catch (MessagingException ex) {
            logger.error(ex.getMessage());
        }

    }
}
