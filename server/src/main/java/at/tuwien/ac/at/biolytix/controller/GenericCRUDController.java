package at.tuwien.ac.at.biolytix.controller;

import at.tuwien.ac.at.biolytix.dto.BaseDto;
import at.tuwien.ac.at.biolytix.exception.ServiceException;
import at.tuwien.ac.at.biolytix.service.ICRUDService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class GenericCRUDController<D extends BaseDto> {

    protected final ICRUDService<D> crudService;


    public GenericCRUDController(ICRUDService<D> crudService) {
        this.crudService = crudService;
    }

    @ApiOperation(value = "Create/save a new entity-dto")
    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces="application/json")
    public ResponseEntity<D> create(@RequestBody D toCreate) throws ServiceException {
        D toReturn = crudService.create(toCreate);
        return new ResponseEntity<>(toReturn, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Retrieve an entity-dto by its ID")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces="application/json")
    public ResponseEntity<D> read(@ApiParam(value = "ID of the requested entity") @PathVariable Integer id) throws ServiceException {
        D toRead = createDtoFromId(id);
        D toReturn = crudService.read(toRead);
        return new ResponseEntity<>(toReturn, HttpStatus.OK);
    }

    @ApiOperation(value = "Retrieve all entity-dtos")
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<D>> readAll() throws ServiceException {
        return new ResponseEntity<>(crudService.readAll(), HttpStatus.OK);
    }


    @ApiOperation(value = "Update an entity-dto, given its ID")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes="application/json", produces="application/json")
    public ResponseEntity<D> update(@RequestBody D toUpdate, @ApiParam(value = "ID of the entity to update") @PathVariable Integer id) throws ServiceException {
        toUpdate.setId(id);
        D toReturn = crudService.update(toUpdate);
        return new ResponseEntity<>(toReturn, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete an entity-dto, given its ID")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    @ResponseStatus(value = HttpStatus.OK, reason = "Either deletion was successful or not, user does not need to be informed")
    public void delete(@ApiParam(value = "ID of the entity to delete") @PathVariable Integer id) throws ServiceException {
        D toDelete = createDtoFromId(id);
        crudService.delete(toDelete);
    }


    protected abstract D createDtoFromId(Integer id);

}