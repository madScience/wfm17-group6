## Installation
1. Download Tomcat https://camunda.org/download/
2. ```mvn clean install``` in this folder (biolytixbpmn)
3. move created .war to downloaded tomcat => 'server/apache-tomcat-8.0.24/webapps'
4. start: ```sh start-camunda.sh``` in root of downloaded tomcat
5. stop: in folder downloaded tomcat folder => 'server/apache-tomcat-8.0.24/bin' ```sh shutdown.sh```
6. the server should run on http://localhost:8080/camunda
7. user: demo, pw: demo is the default user login
8. when rerun ```mvn clean install```, the war has to be recopied, the server does not to be restarted

## Modification of Delegates
Every Delegate works as followed:
- at.tuwien.ac.at.biolytix.bpmn.**Delegate is added to the BPMN as Java Class
- Delegate can be found under at.tuwien.ac.at.biolytix.bpmn
- the analysisId can be accessed via ```int analysisId = (int) execution.getVariable("analysisId");```

