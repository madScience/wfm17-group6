package at.tuwien.ac.at.biolytix.bpmn;

import com.google.gson.Gson;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.logging.Logger;


public class AnalysisRunDelegate implements JavaDelegate {

    private final static int STANDARD_TASK_MIN_TIME = 180;
    private final static int STANDARD_TASK_MAX_TIME = 300;

    private final static int HIGHP_TASK_MIN_TIME = 60;
    private final static int HIGHP_TASK_MAX_TIME = 160;


    private final static Logger LOGGER = Logger.getLogger("AnalysisRunDelegate");

    ExecutorService executor = Executors.newFixedThreadPool(10);
    List<Future<Boolean>> list = new ArrayList<Future<Boolean>>();

    private final class Worker implements Callable<Boolean>{

        private String priority;
        private int taskID;
        private Boolean taskSstate;

        public Worker(int ID, String p) {
            this.taskID = ID;
            this.priority = p;
            this.taskSstate = true;
        }
        //Order o = (Order)

        @Override
        public Boolean call() throws Exception {
            if (!this.priority.equals("STANDARD")) {
                LOGGER.info("HIGH PRIORITY TASK: " + taskID);
                int timeInMinutes = new Random().nextInt((HIGHP_TASK_MAX_TIME - HIGHP_TASK_MIN_TIME) + 1) + HIGHP_TASK_MIN_TIME;
                LOGGER.info("TASK: " + taskID + " is taking:" + timeInMinutes + " MINUTES TO FINISH");
                long timeToWait = TimeUnit.MINUTES.toMillis(timeInMinutes);
                try {
                    synchronized (this) {
                        LOGGER.info("ANALYSIS  " + this.taskID + " STARTING");
                        this.wait(timeToWait);
                        LOGGER.info("ANALYSIS  " + this.taskID + " FINISHED");
                        // for jump to exception reason
                        taskSstate = true;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    taskSstate = false;
                    LOGGER.info("RESTARTING ANALYSIS: " + taskID);
                }

            }else{
                LOGGER.info("STANDARD TASK: " + taskID);
                int timeInMinutes = new Random().nextInt((STANDARD_TASK_MAX_TIME - STANDARD_TASK_MIN_TIME) + 1) + STANDARD_TASK_MIN_TIME;
                LOGGER.info("TASK: " + taskID + " is taking: " + timeInMinutes + " MINUTES TO FINISH");
                long timeToWait = TimeUnit.MINUTES.toMillis(timeInMinutes);
                try {
                    synchronized (this) {
                        LOGGER.info("ANALYSIS  " + this.taskID + " STARTING");
                        this.wait(timeToWait);
                        LOGGER.info("ANALYSIS  " + this.taskID + " FINISHED");
                        // for jump to exception reason
                        taskSstate = true;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    taskSstate = false;
                    LOGGER.info("RESTARTING ANALYSIS: " + taskID);
                }

            }



            return taskSstate;
        }
    }

    /*
    ExecutorService executor = Executors.newFixedThreadPool(10);

    Exception in thread "pool-5-thread-1" org.camunda.bpm.engine.ProcessEngineException: ENGINE-03041 Cannot work with serializers outside of command context.
    at org.camunda.bpm.engine.impl.db.EnginePersistenceLogger.serializerOutOfContextException(EnginePersistenceLogger.java:381)
    at org.camunda.bpm.engine.impl.persistence.entity.util.TypedValueField.getSerializers(TypedValueField.java:208)
    at org.camunda.bpm.engine.impl.persistence.entity.util.TypedValueField.setValue(TypedValueField.java:110)
    at org.camunda.bpm.engine.impl.persistence.entity.VariableInstanceEntity.<init>(VariableInstanceEntity.java:118)
    at org.camunda.bpm.engine.impl.persistence.entity.VariableInstanceEntity.create(VariableInstanceEntity.java:136)
    at org.camunda.bpm.engine.impl.persistence.entity.VariableInstanceEntityFactory.build(VariableInstanceEntityFactory.java:27)
    at org.camunda.bpm.engine.impl.persistence.entity.VariableInstanceEntityFactory.build(VariableInstanceEntityFactory.java:21)
    at org.camunda.bpm.engine.impl.core.variable.scope.AbstractVariableScope.setVariableLocal(AbstractVariableScope.java:316)
    at org.camunda.bpm.engine.impl.core.variable.scope.AbstractVariableScope.setVariable(AbstractVariableScope.java:304)
    at org.camunda.bpm.engine.impl.core.variable.scope.AbstractVariableScope.setVariable(AbstractVariableScope.java:286)
    at at.tuwien.ac.at.biolytix.bpmn.AnalysisRunDelegate$Worker.run(AnalysisRunDelegate.java:71)
    at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
    at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
    at java.lang.Thread.run(Thread.java:748)


    private final class Worker implements Runnable {

        private DelegateExecution context;
        private String priority;
        private int taskID;
        private int taskSstate;

        public Worker(int ID, String p, DelegateExecution context) {
            this.taskID = ID;
            this.priority = p;
            this.context = context;
            this.taskSstate = 0;
        }

        @Override
        public void run() {
            if (this.priority.equals("HIGH_PRIORITY")) {
                // wait for five minutes
                LOGGER.info("HIGH_PRIORITY ANALYSIS");
                LOGGER.info("ANALYSIS  " + this.taskID + " TAKING LONGER");
                try {
                    long timeToWait = TimeUnit.MINUTES.toMillis(5);
                    synchronized (this) {
                        this.wait(timeToWait);
                    }
                    // finished, set to TRUE
                    taskSstate = 1;
                } catch (InterruptedException e) {
                    LOGGER.info("SHIT HAPPENED");
                    e.printStackTrace();
                }
                LOGGER.info("ANALYSIS  " + this.taskID + " WAITING FINISHED");
                context.setVariable("isSuccessfulAnalysis", taskSstate);
            } else {
                LOGGER.info("STANDARD PRIORITY ANALYSIS");
                LOGGER.info("ANALYSIS  " + this.taskID + " TAKING SHORTER");
                try {
                    long timeToWait = TimeUnit.MINUTES.toMillis(2);
                    synchronized (this) {
                        wait(timeToWait);
                    }
                    // finished, set to TRUE
                    taskSstate = 1;
                } catch (InterruptedException e) {
                    LOGGER.info("SHIT HAPPENED");
                    e.printStackTrace();
                }
                //TimeUnit.MINUTES.sleep(2);
                LOGGER.info("ANALYSIS  " + this.taskID + " WAITING FINISHED");
                context.setVariable("isSuccessfulAnalysis", taskSstate);
            }

        }

    }
    */

    public void execute(DelegateExecution execution) throws Exception {
        int analysisId = (Integer) execution.getVariable("analysisId");


        LOGGER.info("RUNNING ANALYSIS FOR ID: " + analysisId);

        String urlAdress = "http://localhost:9020/order/" + analysisId;

        String USER_AGENT = "Mozilla/5.0";
        URL url = new URL(urlAdress);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        Gson gson = new Gson();
        Order order = gson.fromJson(response.toString(), Order.class);

        // HACK to extract priority
        String responseString = response.toString();
        int priorityIndex = responseString.indexOf("\"priority\":");
        // priority string
        String p = responseString.substring(priorityIndex + 11, responseString.indexOf(",", priorityIndex));
        order.setPriority(p);

        if (order.getId() == 0) {
            LOGGER.info("No new order");
        } else {
            int aId = order.getId();
            //order.
            LOGGER.info("The analysisId is: " + aId + " and has PRIORITY: " + order.getPriority());
            execution.setVariable("analysisId", aId);

        }


        // TRY N
        // this is where the magic happens
        LOGGER.info("ADDING analysisId : " + order.getId() + " TO QUEUE ");
        // submit task to queue
        list.add(executor.submit(new Worker(order.getId(), order.getPriority())));

        // wait to finish and update task status
        for(Future<Boolean> futureOrderState : list){
            try {
                LOGGER.info("SETTING RESULT FOR AID: " + order.getId());
                execution.setVariable("isSuccessfulAnalysis", futureOrderState.get());
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        /*
        // add async taks to QUEUE
        //executor.execute(new Worker(order.getId(), order.getPriority(), execution));

        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            LOGGER.info("SHIAT HAPPEND WITH THREADPOOL EXECUTOR");
            e.printStackTrace();
        }
        */

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        /*
        Random r = new Random();
        int Low = 10;
        int High = 100;
        int Result = r.nextInt(High-Low) + Low;

        */
        /*
        LOGGER.info("Run analysis for ID ':" + analysisId);
        Random random = new Random();
        Boolean isSuccessfulAnalysis = random.nextBoolean();

        // if true then the process engine will proceed to the task, if not it will restart this Delegate
        execution.setVariable("isSuccessfulAnalysis", isSuccessfulAnalysis);
        */
    }
}
