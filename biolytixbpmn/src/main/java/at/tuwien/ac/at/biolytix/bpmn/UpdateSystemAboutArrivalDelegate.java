package at.tuwien.ac.at.biolytix.bpmn;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import java.net.URL;
import java.net.HttpURLConnection;



public class UpdateSystemAboutArrivalDelegate implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("UpdateSystemAboutArrivalDelegate");

    public void execute(DelegateExecution execution) throws Exception {
        int analysisId = (Integer) execution.getVariable("analysisId");
        LOGGER.info("NOW WAITING SOME TIME ...");
        //TODO: must be in range (from, to) and also synchronized (@see AnalysisRunUpdateDelegate), otherwise this activity could halt everything
        //Thread.sleep((long)(Math.random() * 3600000));
        LOGGER.info("WAITING FINISHED ...");

        try {
            LOGGER.info("Update and send arrival email for ID: " + analysisId);
            String urlAdress = "http://localhost:9020/order/" + analysisId + "/updateArrivedOrder?id=" + analysisId;
            String USER_AGENT = "Mozilla/5.0";
            URL url = new URL(urlAdress);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            int responseCode = con.getResponseCode();

            LOGGER.info("the result has been updated, with HTTP Return: " + responseCode);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

}
