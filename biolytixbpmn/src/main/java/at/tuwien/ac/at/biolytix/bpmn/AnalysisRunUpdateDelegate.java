package at.tuwien.ac.at.biolytix.bpmn;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

public class AnalysisRunUpdateDelegate implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("AnalysisRunUpdateDelegate");

    public void execute(DelegateExecution execution) throws Exception {

        int analysisId = (Integer) execution.getVariable("analysisId");
        boolean isSuccess = (Boolean) execution.getVariable("isSuccessfulAnalysis");

        LOGGER.info("Update analysis results in DB for ID '" + analysisId);
        try {
            LOGGER.info("AnalysisRunUpdateDelegate for ID: " + analysisId);

            String urlAdress = "http://localhost:9020/order/"+ analysisId +"/updateAnalysisSuccess?id=" + analysisId;
            String USER_AGENT = "Mozilla/5.0";
            URL url = new URL(urlAdress);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            int responseCode = con.getResponseCode();

            LOGGER.info("AnalysisRunUpdateDelegate has been executed, with HTTP Return: " + responseCode);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
