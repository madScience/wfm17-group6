package at.tuwien.ac.at.biolytix.bpmn;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

public class ReviewAndReleaseAnalysisDelegate implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("ReviewAndReleaseAnalysisDelegate");

    public void execute(DelegateExecution execution) throws Exception {
        int analysisId = (Integer) execution.getVariable("analysisId");

        try {
            LOGGER.info("Review and release analysis results for ID: " + analysisId);
            String urlAdress = "http://localhost:9020/order/" + analysisId + "/reviewedAndRelease?id=" + analysisId;
            String USER_AGENT = "Mozilla/5.0";
            URL url = new URL(urlAdress);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("PUT");
            int responseCode = con.getResponseCode();

            LOGGER.info("the result has been released, with HTTP Return: " + responseCode);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
