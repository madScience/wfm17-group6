package at.tuwien.ac.at.biolytix.bpmn;

/**
 * Created by sebastianpinegger on 22/05/17.
 */
public class Order {
    private int id;

    private String priority;


    public int getId () {
        return this.id;
    }

    public void setPriority(String p){
        this.priority = p;
    }

    public String getPriority(){
        return this.priority;
    }

    //private userDetails userDetails;

    /*


    public String getPriority () {return this.userDetails.priority; }


    public class userDetails {
        String title;
        String status;
        String priority;
    }
    */
}
