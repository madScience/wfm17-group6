package at.tuwien.ac.at.biolytix.bpmn;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class UnpackSampleDelegate implements JavaDelegate{
    private final static Logger LOGGER = Logger.getLogger("AnalysisRunDelegate");

    public void execute(DelegateExecution execution) throws Exception {
        int analysisId = (Integer) execution.getVariable("analysisId");
        LOGGER.info("Unpacking the sample with the ID: " + analysisId);

    }
}
