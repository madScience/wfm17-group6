package at.tuwien.ac.at.biolytix.bpmn;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Biolytix BPMN")
public class BiolytixApplication extends ServletProcessApplication {
}
