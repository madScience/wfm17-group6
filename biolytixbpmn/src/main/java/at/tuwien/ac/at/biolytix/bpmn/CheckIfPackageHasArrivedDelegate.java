package at.tuwien.ac.at.biolytix.bpmn;


import com.google.gson.Gson;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;


public class CheckIfPackageHasArrivedDelegate implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("CheckIfPackageHasArrivedDelegate");

    public void execute(DelegateExecution execution) throws Exception {

        // default
        execution.setVariable("hasArrived", false);
        LOGGER.info("Checking if new package has arrived.");

        try {
            // EXAMPLE HTTP CALL
            String urlAdress = "http://localhost:9020/order/getUnarrivedOrder";
            String USER_AGENT = "Mozilla/5.0";
            URL url = new URL(urlAdress);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Gson gson = new Gson();
            Order order = gson.fromJson(response.toString(), Order.class);

            if(order.getId() == 0) {
                LOGGER.info("No new order");
            } else {
                int analysisId = order.getId();
                LOGGER.info("The analysisId is: " + analysisId);
                execution.setVariable("analysisId", analysisId);
                execution.setVariable("hasArrived", true);
            }
        } catch (Exception ex) {
            execution.setVariable("hasArrived", false);
            ex.printStackTrace();
        }
    }
}
