/**
 * Created by Flo on 28.05.2017.
 */
'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:StatusCtrl
 * @description
 * # StatusCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('StatusCtrl', function ($scope, orderTokenRESTService) {
    $scope.tokenReceived = false;
    $scope.order = {};
    $scope.data = {};

    $scope.getOrder = function() {
      orderTokenRESTService.getOrder($scope.data).$promise.then(function (order) {
        console.log(order);
        $scope.tokenReceived = true;
        $scope.order = order;
      });
    };
  });