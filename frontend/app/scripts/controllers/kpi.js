/**
 * Created by Florian on 26.05.2017.
 */
'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:KPICtrl
 * @description
 * # KPICtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('KPICtrl', function ($scope, KPIRESTService) {

    $scope.kpi = {};

    KPIRESTService.getKPIs().$promise.then(function(result) {
      console.log(result);
      $scope.kpi = result;
    });
  });
