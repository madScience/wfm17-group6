'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('MainCtrl', function ($scope, orderRESTService, priorityJSONService) {

    $scope.priority = {};
    $scope.priorities = [];
    $scope.newOrder = {};

    $scope.submitted = false;
    $scope.infoText = {};

    priorityJSONService.query().$promise.then(
      function(priorities) {
        for (var i = 0; i < priorities.length; i++) {
          $scope.priorities[i] = {name: priorities[i].name, value: priorities[i].value};
        }
      });

    $scope.create = function() {
      $scope.newOrder.orderDetails.priority = $scope.priority;
      orderRESTService.makeOrder($scope.newOrder, function() {
        console.log($scope.newOrder);
        $scope.infoText = 'Order successfully submitted!';
        $scope.submitted = true;
      }, function() {
        $scope.infoText = 'There seems to have been a problem while submitted your order...';
      });
    };


  });
