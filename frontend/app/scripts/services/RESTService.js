'use strict';

angular.module('frontendApp')
  .factory('orderRESTService', function ($resource, BIOLYTIX_API) {
    return $resource(BIOLYTIX_API + 'order/:id', {}, {
      makeOrder: {method: 'POST'}
    });
  })
  .factory('KPIRESTService', function($resource, BIOLYTIX_API) {
    return $resource(BIOLYTIX_API + '/order/generateKPIs', {}, {
      getKPIs: {method: 'PUT'}
    });
  })
  .factory('orderTokenRESTService', function ($resource, BIOLYTIX_API) {
    return $resource(BIOLYTIX_API + 'order/token/:token', {}, {
      getOrder: {method: 'GET', params: { 'token' : '@token' }}
    });
  });
