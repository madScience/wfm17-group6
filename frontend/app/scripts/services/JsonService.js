'use strict';

/* REST Services */

angular.module('frontendApp')

  .factory('priorityJSONService', function ($resource) {
    return $resource('json/priority.json');
  });
