# Workflow Modeling 2017 - Group 6 README
## Prerequisites ##
In order to use the program you need to have Java installed (JDK 8) with Maven (https://maven.apache.org/install.html).

With 
```
#!git
git clone https://madScience@bitbucket.org/madScience/wfm17-group6.git

```
you can download this repository onto your computer.
## Database ##
The database, with which the project was set up, is PostgresQL (available here: https://www.postgresql.org/download/)
After having installed the database client, you have to set up the database and role accordingly.

Role / User: wfm_group6
Password: wfm_group6

Database: biolytix_server

On Windows: Start the pgAdminIV oder pgAdminIII program in your postgres bin folder and set up the role with password (as shown above) by simply rightclicking the userrole category in the opened window of postgres and select CREATE or ADD ROLE. When adding the database (right click on database and ADD or CREATE DATABASE) you have to set the owner to the previously created role and set the name of the new database to "biolytix_server".

On Linux: Install via Command Line with following command:

```
#!bash

sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```

Now to setting up the new user and database via the postgres command line interface:

```
#!bash

sudo -i -u postgres

psql

CREATE USER 'wfm_group6' WITH PASSWORD 'wfm_group6';

CREATE DATABASE biolytix_server OWNER 'wfm_group6';
```

## Run the Server program ##
The server is written in Java and set up via Spring-Boot. Change into the 'server' folder and execute 'mvn clean install' to get all the dependencies needed to successfully run it. To run the server, type 'mvn spring-boot:run' into the command line or, if you are using an IDE like IntelliJ or Eclipse, start the Application.java and the server will be started also.

## REST Endpoints ##
The RESTful API Endpoints can be looked at under the following URI when starting the server locally: http://localhost:9020/swagger-ui.html
When you click on an endoint, you can see the layout of the JSON object, that has to be submitted in order to execute the corresponding calls in the backend.

## BPMN Engine ##
Please see README in folder 'biolytixbpmn' folder [BPMN Engine README](biolytixbpmn/README.md)

## Run the Frontend Order Submit Webpage ##
In order to run the frontend page for submitting an order and creating it in the database, node and npm have to be installed.
Run

```
#!bash

npm install -g grunt-cli
npm install -g bower
```
to install the grunt command line interface and bower globally. After that, when you are inside the frontend folder, execute following commands in order:
```
#!bash

npm install
bower install

```
After that, every needed module to run the application should be installed locally and with 
```
#!bash

grunt serve
```
a webserver will be started locally on port 9000 which will be accessible through a web browser of your choice at http://localhost:9000.